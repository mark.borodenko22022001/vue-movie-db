import Vuex from 'vuex'
import films from './films'
import my from './my'
import genre from './genre'
import Vue from 'vue'
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
      films,
      my,
      genre

    }
  });

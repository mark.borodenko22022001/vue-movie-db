import axios from 'axios'

export const getAll = ({ commit })  => {
  axios.get('https://api.themoviedb.org/3/genre/movie/list?api_key=63049dc214fb33db08a5f1af68e8e3a0&language=en-US')
    .then((response) => {
      commit('setGenres', response.data.genres)
    })
};

import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'

const state = {
  genres: [],
  current: null
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}

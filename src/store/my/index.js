import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'

const state = {
  films: localStorage.getItem('myFilms')? JSON.parse(localStorage.getItem('myFilms')): {},
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}

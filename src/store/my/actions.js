import axios from 'axios'



export const addOrRemoveFilm = ({ commit, getters }, film)  => {
  let films = getters.getAll()
  if(films[film.id]) {
    films[film.id] = undefined;
  } else {
    films[film.id] = film;
  }
  localStorage.setItem("myFilms",JSON.stringify(films));
  commit('setFilms',films)
};

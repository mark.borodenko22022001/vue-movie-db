export const getAll = (state) => () => {
  return state.films;
};

export const getList = (state) => () => {
  return Object.keys(state.films).map(index=>state.films[index]);
};

export const isFilm = (state) => (id) => {
  return state.films[id] != undefined
}

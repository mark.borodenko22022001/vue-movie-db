import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'

const state = {
    films: [],
    genre: null,
    carentPage: 1,
    query: '',
    pageCount: 0
}

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
}

export const setFilms = (state, payload) => {
  state.films = payload
};

export const setCarentPage = (state, payload) => {
  state.carentPage = payload
};

export const setTotlePage = (state, payload) => {
  state.pageCount = payload
};

export const setGenre = (state, payload) => {
  state.genre = payload
};

export const setQuery = (state, payload) => {
  state.query = payload
};

export const getAll = (state) => () => {
  return state.films;
};

export const getCarentPage = (state) => () => {
  return state.carentPage;
};

export const getTotlePage = (state) => () => {
  return state.pageCount;
};

export const getGenre = (state) => () => {
  return state.genre;
};

export const getQuery = (state) => () => {
  return state.query;
};

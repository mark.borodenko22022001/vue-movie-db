import axios from 'axios'

const BY_GENRE = 'https://api.themoviedb.org/3/discover/movie?api_key=63049dc214fb33db08a5f1af68e8e3a0';
const BY_QUERY = 'https://api.themoviedb.org/3/search/movie?api_key=63049dc214fb33db08a5f1af68e8e3a0';
function filterBuilder(filter) {
  let builder ='&page='+filter.page;
  if ( filter.genre ) {
    builder+= '&with_genres='+filter.genre
  }
  if ( filter.query !== '' ) {
    builder+= '&query='+filter.query
  }
  return builder;
}


export const getFilms = ({ commit, getters }, filter)  => {
  axios
    .get(
      `${getters.getQuery() !== '' ? BY_QUERY: BY_GENRE  }${filterBuilder({
        page: getters.getCarentPage(),
        genre: getters.getGenre(),
        query: getters.getQuery().toLowerCase()
      })}`
    )
    .then(response => {
      commit('setFilms', response.data.results);
      commit('setCarentPage', response.data.page );
      commit('setTotlePage', response.data.total_pages);
    });
};

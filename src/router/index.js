import Vue from 'vue'
import Router from 'vue-router'
import MainLayout from '@/views/layout/MainLayout'
import Movie from '@/views/movies/Movie'
import FavoriteList from '@/views/favorite/FavoriteList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: MainLayout
    },
    {
      path: '/movie/:id',
      name: 'Movie',
      component: Movie
    },
    {
      path: '/FavoriteList',
      name: 'FavoriteList',
      component: FavoriteList
    }
    ],
  mode: 'history'
})
